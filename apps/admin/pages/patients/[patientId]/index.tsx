import {
    Container,
    Center,
    Spinner,
    Text,
    useToast,
    Textarea,
    Tabs,
    TabList,
    TabPanels,
    TabPanel,
    Tab
} from "@chakra-ui/react";
import {
    GetAppointmentBlanksOfUserDocument,
    useEditSessionBlankMutation,
    useGetActiveSessionByUserIdQuery,
    useGetAppointmentBlanksOfUserQuery,
    useGetUserByIdQuery,
    useUploadSessionBlankMutation,
} from "@lucem/shared-gql";
import {
    CreateAppointmentBlankFormSchema,
    EditAppointmentBlankFormSchema,
} from "@lucem/ui/components/organisms/SessionModal/shared/initialValues";
import PatientPage from "@lucem/ui/pages/PatientPage";
import { getDoctorTokens } from "@lucem/ui/utils/getTokens";
import _ from "lodash";
import { useRouter } from "next/router";
import { GET_APPOINTMENT_BLANKS_OF_USER } from "@lucem/shared-gql/queries/getAppointmentResults";
import React, {useEffect, useState} from "react";
import {PatientEntity} from "@core/types/patient/IPatient";
import {ElevatedContainer} from "@lucem/ui/components/atoms/ElevatedContainer";

const PatientPageContainer = () => {
    const router = useRouter();
    const { token, doctorId } = getDoctorTokens();
    const toast = useToast();
    const patientId = router.query.patientId as string;
    const [patient, setPatient] = useState<PatientEntity[]>([])
    useEffect(()=>{
        console.log(router.query)
        setPatient(JSON.parse(router.query.title))
        console.log(JSON.parse(router.query.title))
    },[])

    const { loading: userByIdloading, data: getUserByIdRes } =
        useGetUserByIdQuery({
            variables: {
                userId: patientId,
            },
        });
    const { loading: activeSessionLoading, data: getActiveSessionRes } =
        useGetActiveSessionByUserIdQuery({
            variables: {
                userId: patientId,
            },
        });
    const {
        loading: appointmentBlanksLoading,
        data: appointmentBlanksRes,
        refetch,
    } = useGetAppointmentBlanksOfUserQuery({
        variables: {
            userId: patientId,
        },
        context: {
            headers: { Authorization: token },
        },
    });
    const [uploadSessionBlank, { loading }] = useUploadSessionBlankMutation({
        context: {
            headers: { Authorization: token },
        },
        refetchQueries: [{ query: GET_APPOINTMENT_BLANKS_OF_USER }],
    });
    const [editSessionBlank, { loading: editLoaing }] =
        useEditSessionBlankMutation({
            context: {
                headers: { Authorization: token },
            },
            refetchQueries: [{ query: GET_APPOINTMENT_BLANKS_OF_USER }],
        });

    if (userByIdloading || activeSessionLoading) {
        return (
            <Container>
                <Center>
                    <Spinner></Spinner>
                </Center>
            </Container>
        );
    }
    const onCreateAppointmentBlank = async (
        values: CreateAppointmentBlankFormSchema,
    ) => {
        const { complaint, diagnose, inspections } = values;
        diagnose.preliminary = diagnose.preliminary ? true : false;
        const { data, errors } = await uploadSessionBlank({
            variables: {
                complaint,
                diagnose,
                inspections,
                sessionId: activeSession._id,
            },
            onCompleted() {
                toast({
                    status: "success",
                    title: "Создание приема завершено!",
                });
            },
        });
        refetch();
    };
    const onEditAppointmentBlank = async (
        values: EditAppointmentBlankFormSchema,
    ) => {
        const payload = _.removeDeepByKey(values, "doctor");
        payload.inspections.data = values.inspections.data.map((el) => {
            let result = _.omit(el, ["doctor", "doctorId", "_id"]);

            if (el?.images[0]?.m) {
                result = _.omit(result, "images");
                result.photoURL = el.images[0];
                return result;
            }
            el.photoURL = [];
            return el;
        });
        const copy = { ...payload };
        const { complaint, diagnose, inspections } = copy;
        diagnose.preliminary = diagnose.preliminary ? true : false;
        await editSessionBlank({
            variables: {
                complaint,
                diagnose,
                inspections,
                appointmentBlankId: values.appointmentBlankId,
            },
            onCompleted() {
                toast({
                    status: "success",
                    title: "Редактирование завершено!",
                });
            },
            refetchQueries: [{ query: GET_APPOINTMENT_BLANKS_OF_USER }],
        });
        refetch();
    };

    //const patient = getUserByIdRes.getUserByID;
    //const activeSession = getActiveSessionRes?.getActiveSessionByUserId;
    //const appointmentBlanks = appointmentBlanksRes?.getAppointmentBlanksOfUser;
    return (
        <>
            <ElevatedContainer className="rounded-lg p-4 h-full">
                <div
                    style={{ marginBottom: 20 }}
                    className="flex items-center "
                >
                    <div className="avatar mr-4">
                        <div className="rounded-full w-20 h-20 bg-primary">
                            {patient.photoURL.length > 0 ? (
                                <img src={patient.photoURL} />
                            ) : (
                                <span className="text-4xl text-white uppercase flex items-center justify-center h-full">
                                    {patient.fullName[0]}
                                </span>
                            )}
                        </div>
                    </div>
                    <div style={{width:"100%"}} className="flex flex-col justify-start">
                        <span className="text-2xl font-medium w-1/2 h-16">
                            {patient.fullName}
                        </span>
                        <span className="text-base-300">
                            ИИН: {patient.identifyNumber}
                        </span>
                    </div>
                </div>
                <div className={"flex flex-col"}>
                    <span className="text-xl mb-1 font-medium">
                        {" "}
                        Данные пациента{" "}
                    </span>
                    <div className={"flex flex-row"}>
                        <div className={"pr-5"}>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Адрес:{" "}
                                </span>
                                <div className="text-base-300">
                                    {" "}
                                    {patient.address}
                                </div>
                            </div>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Дата рождение:{" "}
                                </span>
                                <div className="text-base-300">
                                    {" "}
                                    {patient.dateOfBirth}
                                </div>
                            </div>
                        </div>
                        <div className={"pl-5"}>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Телефон номера:{" "}
                                </span>
                                <div className="text-base-300">
                                    {" "}
                                    {patient.phoneNumber}
                                </div>
                            </div>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Электронная почта:{" "}
                                </span>
                                <div className="text-base-300">
                                    {" "}
                                    {patient.email}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"flex flex-col"}>
                        <div>
                    <span className="text-xl mb-1 font-medium">
                            {" "}
                        Результаты анализов{" "}
                        </span>
                            <span > Посмотреть все
                    </span>
                        </div>


                        <div className={"flex flex-col"}>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Терапевт:{" "}
                                </span>
                                <div className="text-base-300">
                                    {" "}
                                    {patient.doctor}
                                </div>
                            </div>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Диагноз:{" "}
                                </span>
                                <div className="text-base-300">
                                    {" "}
                                    {patient.diagnoz}
                                </div>
                            </div>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Заключение:{" "}
                                </span>

                                <div className="text-base-300">
                                    <Textarea defaultValue={patient.assignment}/>
                                </div>
                            </div>
                            <div
                                style={{ marginBottom: 15 }}
                                className={"flex flex-col"}
                            >
                                <span
                                    style={{ fontSize: "18px" }}
                                    className="font-medium"
                                >
                                    {" "}
                                    Назначение:{" "}
                                </span>

                                <div className="text-base-300">
                                    <Textarea defaultValue={patient.assignment}/>
                                </div>
                            </div>
                        </div>
                        <button
                            style={{
                                margin: "0 auto",
                                borderRadius: "16px",
                                padding: "5px",
                                border: "1px solid #000",
                                width: "200px",
                            }}
                            onClick={(e) => {
                                e.preventDefault();
                                router.push('/patients')
                            }}
                        >
                            Сохранить
                        </button>
                    </div>
                </div>
            </ElevatedContainer>
            <ElevatedContainer className="rounded-lg p-4 h-full">
                <span className="text-2xl font-medium w-1/2 h-16 mb-5">
                            История пациента
                        </span>
                <Tabs>
                    <TabList>
                        <Tab>Календарь</Tab>
                        <Tab>Назначенные лекарства</Tab>
                    </TabList>
                    <TabPanels>
                        <TabPanel>
                            <div style={{display:"flex", gap:20, flexDirection:"column"}}>

                            <div style={{display:"flex", gap:20}}>
                                <div style={{border:"1px solid #000", width:200, height:200, borderRadius:10, display:"flex", flexDirection:"column", alignItems:"center", justifyContent:"center"}}>
                                    <div style={{fontSize:32}}>
                                        2018
                                    </div>
                                    <div style={{display:"flex", gap:10}}>
                                        <div style={{width:20, height:20, borderRadius:"50%", background:"#f56666"}}>

                                        </div>
                                        <div style={{width:20, height:20, borderRadius:"50%", background:"#3fb92b"}}>

                                        </div>
                                        <div style={{width:20, height:20, borderRadius:"50%", background:"#144c8d"}}>

                                        </div>
                                    </div>
                                </div>
                                <div style={{border:"1px solid #000", width:200, height:200, borderRadius:10, display:"flex", flexDirection:"column", alignItems:"center", justifyContent:"center"}}>
                                    <div style={{fontSize:32}}>
                                        2019
                                    </div>
                                    <div style={{display:"flex", gap:10}}>
                                        <div style={{width:20, height:20, borderRadius:"50%", background:"#f56666"}}>

                                        </div>
                                        <div style={{width:20, height:20, borderRadius:"50%", background:"#3fb92b"}}>

                                        </div>
                                    </div>
                                </div>

                                <div style={{border:"1px solid #000", width:200, height:200, borderRadius:10, display:"flex", flexDirection:"column", alignItems:"center", justifyContent:"center"}}>
                                    <div style={{fontSize:32}}>
                                        2020
                                    </div>
                                    <div style={{display:"flex", gap:10}}>

                                    </div>
                                </div>

                            </div>
                            <div style={{display:"flex", gap:20}}>
                                <div style={{border:"1px solid #000", width:200, height:200, borderRadius:10, display:"flex", flexDirection:"column", alignItems:"center", justifyContent:"center"}}>
                                    <div style={{fontSize:32}}>
                                        2021
                                    </div>
                                    <div style={{display:"flex", gap:10}}>

                                    </div>
                                </div>
                                <div style={{border:"1px solid #000", width:200, height:200, borderRadius:10, display:"flex", flexDirection:"column", alignItems:"center", justifyContent:"center"}}>
                                    <div style={{fontSize:32}}>
                                        2022
                                    </div>
                                    <div style={{display:"flex", gap:10}}>
                                        <div style={{width:20, height:20, borderRadius:"50%", background:"#f56666"}}>

                                        </div>

                                    </div>
                                </div>
                                <div style={{border:"1px solid #000", width:200, height:200, borderRadius:10, display:"flex", flexDirection:"column", alignItems:"center", justifyContent:"center"}}>
                                    <div style={{fontSize:32}}>
                                        2023
                                    </div>
                                    <div style={{display:"flex", gap:10}}>

                                    </div>
                                </div>
                            </div>
                            </div>

                        </TabPanel>
                        <TabPanel>
                            <p>Бедаквилин</p>
                            <p>Безлотоксумаб</p>
                            <p>Мебгидролин</p>
                            <p>Ципросин</p>
                        </TabPanel>
                    </TabPanels>
                </Tabs>
            </ElevatedContainer>
        </>

    );
};

export default PatientPageContainer;
