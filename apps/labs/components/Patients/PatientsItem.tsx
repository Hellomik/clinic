import { NextPage } from "next"
import Image from "next/image"
import Link from "next/link"
import { testPatientsData } from "pages/patients"

const PatientItem: NextPage<{ data: typeof testPatientsData[0] }> = (props) => {
	return (
		<div className="p-4 w-xxl shadow-first rounded-xl">
			<PatientsItemHeader data={props.data} />
			<PatientsPageItemHeaderAboutClient data={props.data} />
			<PatientItemChooseServices services={props.data.services} />
			<div className="mt-4">
				<div className="font-bold text-lg mb-1">Дата и время</div>
				<div className="">{props.data.dataTime}</div>
			</div>
		</div>
	)
}

export const PatientsItemHeader: NextPage<{ data: typeof testPatientsData[0] }> = (props) => {
	return (
		<Link href={`patients/${props.data.id}`}>
			<div className="flex items-center gap-4 cursor-pointer">
				<div className="">
					{props.data.image
						? <Image src={props.data.image} alt="" className="w-20 h-20 rounded-full" />
						: <div className="w-20 h-20 bg-purple-700 rounded-full"></div>
					}
				</div>
				<div className="">
					<div className="text-xl font-semibold">{props.data.name}</div>
					<div className="text-xl font-semibold">{props.data.surname}</div>
				</div>
			</div>
		</Link>
	)
}

export const PatientsPageItemHeaderAboutClient: NextPage<{ data: typeof testPatientsData[0] }> = (props) => {
	return (
		<div className="mt-3">
			<div className="font-bold text-lg mb-1">Данные пациента</div>
			<div className="flex gap-10">
				<div>
					<div className="">
						<div className="font-bold font-[18px]">Адрес:</div>
						<span>{props.data.address}</span>
					</div>
					<div className="mt-5">
						<div className="font-bold">Дата рождения:</div>
						<span>{props.data.dateOfBirth}</span>
					</div>
				</div>
				<div>
					<div className="">
						<div className="font-bold">Номер телефона:</div>
						<span>{props.data.telNumber}</span>
					</div>
					<div className="mt-5">
						<div className="font-bold">Электронная почта:</div>
						<span>{props.data.email}</span>
					</div>
				</div>
			</div>
		</div>
	)
}

export const PatientItemChooseServices: NextPage<{ services: Array<{ name: string, isChoose: boolean }> }> = (props) => {
	return (
		<div className="mt-4">
			<div className="font-bold text-lg mb-1">Выбранные услуги</div>
			<div className="flex flex-col gap-2">
				{props.services.map((service, index) => {
					return (
						<div key={index} className="flex gap-2 items-center">
							{service.name}
							{service.isChoose
								? <img src="https://img.icons8.com/parakeet/48/null/checked-checkbox.png" className="w-5 h-5" />
								: <img src="https://img.icons8.com/color/48/null/close-window.png" className="w-5 h-5" />
							}
						</div>
					)
				})}
			</div>
		</div>
	)
}

export default PatientItem