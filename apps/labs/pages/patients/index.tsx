import getAdminLayout from "components/layouts/adminLayout";
import React, { useEffect } from "react";

import { PatientsPageComponent } from "@lucem/ui";
import { useGetAllUsersQuery } from "@lucem/shared-gql";
import { useRouter } from "next/router";
import { Center, Container, Spinner } from "@chakra-ui/react";
import PatientItem from "components/Patients/PatientsItem";

export const testPatientsData = [
    {
        id: 1,
        image: null as string | null,
        name: 'Рахат',
        surname: 'Баймухамедов',
        IIN: '000115781332',
        address: 'г Алматы, улица Жуманова 46',
        telNumber: '87759551015',
        dateOfBirth: '2000-01-16',
        email: 'textuser@gmail.com',
        services: [
            {
                name: 'Услуга 1',
                isChoose: false
            },
            {
                name: 'Услуга 2',
                isChoose: true
            },
            {
                name: 'Услуга 3',
                isChoose: true
            },
            {
                name: 'Услуга 4',
                isChoose: true
            },
            {
                name: 'Услуга 5',
                isChoose: false
            },
        ],
        dataTime: '2023-01-25 10:45'
    },
    {
        id: 2,
        image: null as string | null,
        name: 'Бекзат',
        surname: 'Жарылкасын',
        IIN: '546115787792',
        address: 'г Алматы, улица Абоя 98',
        telNumber: '87754679190',
        dateOfBirth: '1996-02-28',
        email: 'textuser2@gmail.com',
        services: [
            {
                name: 'Услуга 1',
                isChoose: true
            },
            {
                name: 'Услуга 2',
                isChoose: false
            },
            {
                name: 'Услуга 3',
                isChoose: false
            },
            {
                name: 'Услуга 4',
                isChoose: false
            },
            {
                name: 'Услуга 5',
                isChoose: false
            },
        ],
        dataTime: '2023-02-01 8:00'
    },
]

const PatientsPage: React.FC = () => {
    const { data, loading } = useGetAllUsersQuery();
    if (loading) {
        return (
            <Container>
                <Center>
                    <Spinner></Spinner>
                </Center>
            </Container>
        );
    }

    return (
        <div className="flex gap-8">
            {testPatientsData.map((item, index) => <PatientItem key={index} data={item} />)}
        </div>
    );
};

export default PatientsPage;
