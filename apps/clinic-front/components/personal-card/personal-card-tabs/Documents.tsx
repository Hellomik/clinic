const Documents = () => {
	return (
		<div className="">
			<h2 className="text-xl my-4">Прикрепите файл</h2>
			<div className="">
				<input type="file" id="file" className="hidden" />
				<label htmlFor="file" className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white">Выбрать файл</label>
			</div>
		</div>
	)
}

export default Documents