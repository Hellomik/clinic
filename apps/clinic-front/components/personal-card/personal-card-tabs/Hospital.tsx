import { PDFDownloadLink } from "@react-pdf/renderer"
import { NextPage } from "next"
import PDFfile from "../PDFfile"

const Hospital = () => {
	return (
		<div className="w-full md:w-10/12 flex flex-col gap-6">
			<ObservationDiary />
			<HospitalDocuments />
			<DischargeEpicrisis />
			<MilestoneEpicrisis />
		</div>
	)
}

const ObservationDiary = () => {
	const observationDiaryData = [
		{
			date: '03.01.2022',
			text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem sapiente eius magni animi inventore. Sunt quaerat hic voluptatibus accusantium illum asperiores, quidem dignissimos officia vel optio beatae placeat adipisci incidunt!"
		},
		{
			date: '24.11.2022',
			text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem sapiente eius magni animi inventore. Sunt quaerat hic voluptatibus accusantium illum asperiores."
		},
		{
			date: '02.01.2023',
			text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit."
		}
	]

	return (
		<div className="">
			<h2 className="text-xl my-3">Дневник наблюдений</h2>
			<div className="border-1 border-black rounded-sm">
				{observationDiaryData.map((item, index) => {
					return (
						<div key={index} className={`flex ${index !== 0 ? 'border-t-1 border-black' : ''}`}>
							<div className="p-2 border-r-1 border-black min-w-20 md:min-w-30 flex justify-center items-center">{item.date}</div>
							<div className="p-2">{item.text}</div>
						</div>
					)
				})}
			</div>
		</div>
	)
}

const HospitalDocuments = () => {
	const hospitalDocumentsData = [
		{
			title: 'Больничный лист 1',
			text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius, culpa nulla consectetur temporibus quibusdam alias incidunt tenetur laboriosam placeat modi. Inventore magnam cupiditate veniam ipsam expedita, ea ut nobis eveniet?"
		},
		{
			title: 'Больничный лист 2',
			text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro harum doloremque quae ea nesciunt aperiam suscipit consequatur sapiente. Earum eum corporis, repellat dolorum dolores ipsum harum fuga nemo totam velit."
		},
	]

	return (
		<div className="">
			<h2 className="text-xl my-3">Больничные листы</h2>
			<div className="flex flex-col gap-4">
				{hospitalDocumentsData.map((item, index) => <HospitalDocumentsItem key={index} item={item} />)}
			</div>
		</div>
	)
}

const HospitalDocumentsItem: NextPage<{ item: { title: string, text: string } }> = (props) => {
	return (
		<div className="p-4 shadow-first rounded-lg">
			<div className="text-lg mb-3">{props.item.title}</div>
			<div className="">{props.item.text}</div>
			<PDFDownloadLink document={<PDFfile title={props.item.title} text={[props.item.text]} />} fileName="document" >
				{({ loading }) => {
					return (
						<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4">
							{!loading
								? 'Скачать PDF'
								: 'Загрузка файла...'
							}
						</button>
					)
				}}
			</PDFDownloadLink>
		</div>
	)
}

const DischargeEpicrisis = () => {
	let text = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor impedit optio corporis officiis alias, delectus obcaecati sapiente porro quas vel. Odit, eligendi quos possimus debitis eius laborum fugit voluptates exercitationem."

	return (
		<div className="">
			<h2 className="text-xl my-3">Выписной эпикриз</h2>
			<div className="">{text}</div>
			<PDFDownloadLink document={<PDFfile title='Выписной эпикриз' text={[text]} />} fileName="document" >
				{({ loading }) => {
					return (
						<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4">
							{!loading
								? 'Скачать PDF'
								: 'Загрузка файла...'
							}
						</button>
					)
				}}
			</PDFDownloadLink>
		</div>
	)
}

const MilestoneEpicrisis = () => {
	return (
		<div className="">
			<h2 className="text-xl my-3">Этапный эпикриз</h2>
			<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia sapiente, nihil nemo natus corrupti magnam dolore in assumenda nam ex facere rem officia quasi eum reiciendis, atque perferendis cumque nostrum.</div>
		</div>
	)
}

export default Hospital