import { NextPage } from "next"
import { useState } from "react"


const CallHistory: NextPage<{ callHistory: any }> = (props) => {
	const [callHistoryData, setCallHistoryData] = useState(props.callHistory)

	return (
		<div className="">
			<h2 className="text-xl my-3">Истории вызовов СМП</h2>
			<div className="flex flex-col gap-4">
				{callHistoryData.map((item, index) => <CallHistoryItem key={index} item={item} />)}
			</div>
		</div>
	)
}


const CallHistoryItem: NextPage<{ item: any }> = (props) => {
	return (
		<div className="shadow-first p-4 rounded-lg">
			<div className="font-semibold mb-3">{props.item.dateAndTime}</div>
			<div className="flex flex-col gap-3">
				<div className="flex flex-col gap-1"><span className="font-semibold">Причина вызова:</span> {props.item.reasonForCalling}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Анамнез:</span> {props.item.anamnesis}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Первичный диагноз:</span> {props.item.primaryDiagnosis}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Процедуры:</span> {props.item.procedures}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Назначения:</span> {props.item.destination}</div>
				<div className=""><span className="font-semibold">Госпитализация:</span> {props.item.hospitalization ? 'Да' : 'Нет'}</div>
			</div>
		</div>
	)
}

export default CallHistory