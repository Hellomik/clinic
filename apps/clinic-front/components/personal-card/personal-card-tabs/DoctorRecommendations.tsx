import { PDFDownloadLink } from "@react-pdf/renderer"
import PDFfile from "../PDFfile"

const DoctorRecommendations = () => {
	const doctorRecommendationsData = [
		"Lorem ipsum dolor, sit amet consectetur adipisicing elit. Incidunt similique quis asperiores autem rem libero officia non voluptate fugiat facilis? Iste tempora modi, nobis cum ut repudiandae perspiciatis ipsam voluptate?",
		"Lorem, ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus incidunt alias fugiat libero rerum dignissimos doloribus adipisci voluptatibus aut ab eum dolorem, tenetur odit, ipsum eaque blanditiis nihil perspiciatis corporis!"
	]

	return (
		<div className="">
			<h2 className="text-xl my-3">Рекомендации врача</h2>
			<div className="">
				{doctorRecommendationsData.map((item, index) => <div key={index} className='mt-3'>{item}</div>)}
			</div>
			<PDFDownloadLink document={<PDFfile title="Рекомендации врача" text={doctorRecommendationsData} />} fileName="recommendations" >
				{({ loading }) => {
					return (
						<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4">
							{!loading
								? 'Скачать PDF'
								: 'Загрузка файла...'
							}
						</button>
					)
				}}
			</PDFDownloadLink>
		</div>
	)
}

export default DoctorRecommendations