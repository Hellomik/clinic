import { Box, Typography, Tabs, Tab } from "@mui/material";
import { useState } from "react";
import AboutFamily from "./personal-card-tabs/AboutFamily";
import CallHistory from "./personal-card-tabs/CallHistory";
import DispensaryRegistration from "./personal-card-tabs/DispensaryRegistration";
import DoctorRecommendations from "./personal-card-tabs/DoctorRecommendations";
import Documents from "./personal-card-tabs/Documents";
import Hospital from "./personal-card-tabs/Hospital";
import ReferralForTests from "./personal-card-tabs/ReferralForTests";

interface TabPanelProps {
	children?: React.ReactNode;
	index: number;
	value: number;
}

function TabPanel(props: TabPanelProps) {
	const { children, value, index, ...other } = props;

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box sx={{ p: 0 }}>
					<Typography component='div'>{children}</Typography>
				</Box>
			)}
		</div>
	);
}

function a11yProps(index: number) {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`,
	};
}

const PersonalCardTabs = () => {
	const [value, setValue] = useState(0)
	const [userData, setUserData] = useState({
		callHistory: [
			{
				dateAndTime: '06.12.2022, 10:45 - 11:00',
				reasonForCalling: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, harum?",
				anamnesis: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sit laboriosam ullam nostrum consequatur dignissimos!",
				primaryDiagnosis: "Lorem, ipsum.",
				procedures: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
				destination: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime eveniet corrupti in debitis deserunt nemo similique aliquam dolore repudiandae omnis. Modi optio quasi facilis corporis earum omnis eum incidunt repellendus?",
				hospitalization: true
			},
			{
				dateAndTime: '03.01.2023, 12:47 - 13:23',
				reasonForCalling: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, harum?",
				anamnesis: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sit laboriosam ullam nostrum consequatur dignissimos!",
				primaryDiagnosis: "Lorem, ipsum.",
				procedures: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
				destination: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime eveniet corrupti in debitis deserunt nemo similique aliquam dolore repudiandae omnis. Modi optio quasi facilis corporis earum omnis eum incidunt repellendus?",
				hospitalization: false
			},
		]
	})

	const handleChange = (event: React.SyntheticEvent, newValue: number) => {
		setValue(newValue);
	}

	return (
		<div
			className="mt-[80px] w-[100%]"
		>
			<Box sx={{ borderBottom: 1, borderColor: '#C182F9' }}>
				<Tabs
					value={value}
					variant='scrollable'
					allowScrollButtonsMobile
					onChange={handleChange}
					aria-label="basic tabs example"
					TabIndicatorProps={{ sx: { backgroundColor: '#C182F9' } }}
					sx={{
						"& button": { color: 'black !important', fontFamily: 'Sono', fontSize: { xs: '14px', sm: '18px' }, textTransform: 'none' },
						// "& button.Mui-selected": { color: '#C182F9 !important' },
					}}
				>
					<Tab label='Календарь' {...a11yProps(0)} />
					<Tab label='Направления' {...a11yProps(1)} />
					<Tab label='Документы' {...a11yProps(2)} />
					<Tab label='О семье' {...a11yProps(3)} />
					<Tab label='Истории вызовов СМП' {...a11yProps(4)} />
					<Tab label='Рекомендации врача' {...a11yProps(5)} />
					<Tab label='Рецепты' {...a11yProps(6)} />
					<Tab label='Календарь беременности' {...a11yProps(7)} />
					<Tab label='Диспансерный учет' {...a11yProps(8)} />
					<Tab label='Направление на анализы' {...a11yProps(9)} />
					<Tab label='Стационар' {...a11yProps(10)} />
				</Tabs>
			</Box>
			<TabPanel value={value} index={0}>
				Календарь
			</TabPanel>
			<TabPanel value={value} index={1}>
				Направления
			</TabPanel>
			<TabPanel value={value} index={2}>
				<Documents />
			</TabPanel>
			<TabPanel value={value} index={3}>
				<AboutFamily />
			</TabPanel>
			<TabPanel value={value} index={4}>
				<CallHistory callHistory={userData.callHistory} />
			</TabPanel>
			<TabPanel value={value} index={5}>
				<DoctorRecommendations />
			</TabPanel>
			<TabPanel value={value} index={6}>
				Рецепты
			</TabPanel>
			<TabPanel value={value} index={7}>
				Календарь беременности
			</TabPanel>
			<TabPanel value={value} index={8}>
				<DispensaryRegistration />
			</TabPanel>
			<TabPanel value={value} index={9}>
				<ReferralForTests />
			</TabPanel>
			<TabPanel value={value} index={10}>
				<Hospital />
			</TabPanel>
		</div>
	);
}

export default PersonalCardTabs