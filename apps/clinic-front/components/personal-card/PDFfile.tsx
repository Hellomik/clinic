import { Page, Text, Image, Document, StyleSheet, Font, } from "@react-pdf/renderer";
import { NextPage } from "next";

Font.register({
	family: "Roboto",
	src: "https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-medium-webfont.ttf"
});

const styles = StyleSheet.create({
	body: {
		padding: 20,
		fontFamily: 'Roboto'
	},
	text: {
		fontSize: '14px'
	}
})

interface IPDFfile {
	title: string
	text: string[]
}

const PDFfile: NextPage<IPDFfile> = (props) => {
	return (
		<Document>
			<Page style={styles.body}>
				<Text style={{ fontSize: 20, marginBottom: 20 }}>{props.title}</Text>
				{props.text.map((item, index) => {
					return (
						<div style={{ marginBottom: 14 }}>
							<Text key={index} style={styles.text}>{item}</Text>
						</div>
					)
				})}
			</Page>
		</Document>
	)
}

export default PDFfile