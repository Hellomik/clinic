import { TextField } from "@mui/material"
import { NextPage } from "next"
import { useState } from "react"
import styled from "styled-components"

const Labs = () => {
	const labsData = [
		{
			name: 'Лаборотория 1',
			services: ['Услуга 1', 'Услуга 2', 'Услуга 3', 'Услуга 4', 'Услуга 5'],
			price: '300$'
		},
		{
			name: 'Лаборотория 2',
			services: ['Услуга 1', 'Услуга 2', 'Услуга 3', 'Услуга 4', 'Услуга 5'],
			price: '100$'
		},
		{
			name: 'Лаборотория 3',
			services: ['Услуга 1', 'Услуга 2', 'Услуга 3', 'Услуга 4', 'Услуга 5'],
			price: '900$'
		}
	]

	return (
		<div className="container mb-8">
			<AboutContainer className="rounded-xl p-8">
				<h1 className="text-xl font-bold mb-4">Лаборотории</h1>
				<div className="flex flex-wrap gap-12">
					{labsData.map((item, index) => <LabsItem key={index} data={item} />)}
				</div>
				<div className="mt-8">
					<div className="text-xl font-semibold mb-3">Инструкции</div>
					<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius asperiores corporis quas nihil, quis, tempore nemo velit commodi odit aut cum et modi neque obcaecati. Necessitatibus vero officia quos?</div>
				</div>
				<div className="mt-8">
					<div className="text-xl font-semibold mb-3">Памятки</div>
					<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius asperiores corporis quas nihil, quis, tempore nemo velit commodi odit aut cum et modi neque obcaecati. Necessitatibus vero officia quos?</div>
				</div>
			</AboutContainer>
		</div>
	)
}

const LabsItem: NextPage<{ data: { name: string, services: string[], price: string } }> = (props) => {
	const [isRecordModal, setIsRecordModal] = useState(false)
	const [isRecorded, setIsRecorded] = useState(false)

	const openModal = () => {
		setIsRecordModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsRecordModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="">
			<div className="text-lg font-semibold">{props.data.name}</div>
			<div className="my-2">
				<div className="font-semibold my-1">График</div>
				<div className="">
					<div className="">пон 10:00 - 16:00</div>
					<div className="">вт 8:00 - 12:00</div>
					<div className="">ср 10:00 - 18:00</div>
					<div className="">чт 8:00 - 16:00</div>
					<div className="">пн 10:00 - 16:00</div>
				</div>
			</div>
			<div className="">
				<div className="font-semibold my-1">Услуги:</div>
				{props.data.services.map((item, index) => <div key={index}>- {item}</div>)}
			</div>
			<div className="my-3">Цена услуг: {props.data.price}</div>
			{isRecordModal &&
				<LabsItemRecordModal services={props.data.services} setIsRecorded={setIsRecorded} closeModal={closeModal} />
			}
			<button className="bg-pink-purple rounded-lg w-32 text-white block" onClick={openModal}>{!isRecorded ? 'Записаться' : 'Вы записаны'}</button>
		</div>
	)
}

interface ILabsItemRecordModal {
	services: string[]
	setIsRecorded: (value: any) => void
	closeModal: () => void
}

const LabsItemRecordModal: NextPage<ILabsItemRecordModal> = (props) => {
	const record = () => {
		props.closeModal()
		props.setIsRecorded(true)
	}

	return (
		<div className="fixed w-full h-full top-0 left-0 bg-modal-black flex justify-center items-center">
			<div className="bg-white p-6 rounded-xl">
				<div className="text-xl mb-4">Заполните информацию</div>
				<div className="">
					<div className="mb-2 font-medium">Выбор услуг</div>
					<div className="flex flex-col gap-2">
						{props.services.map((item, index) => {
							return (
								<div key={index} className="flex gap-2">
									<input type="checkbox" />
									<span>{item}</span>
								</div>
							)
						})}
					</div>
				</div>
				<TextField
					id="datetime-local"
					label="Выберите время"
					type="datetime-local"
					defaultValue="2017-05-24T10:30"
					sx={{ width: 250, my: '30px' }}
					InputLabelProps={{
						shrink: true,
					}}
				/>
				<div className="flex gap-2">
					<button className="bg-pink-purple rounded-lg w-32 text-white block" onClick={props.closeModal}>Отмена</button>
					<button className="bg-pink-purple rounded-lg w-32 text-white block" onClick={record}>Записаться</button>
				</div>
			</div>
		</div>
	)
}

const AboutContainer = styled.div`
    /* margin-left: 10vw;
    margin-right: 10vw;
    margin-top: 46px;
    padding: 60px 100px; */
    background-color: #ffffff;
    color: black;
    border-radius: 20px;
    box-shadow: -10px 10px 20px 20px rgba(110, 1, 215, 0.1);
`;

export default Labs