import Labs from "components/labs/Labs"
import Navbar from "components/Navbar"

const LabsPage = () => {
	return (
		<>
			<Navbar />
			<Labs />
		</>
	)
}

export default LabsPage