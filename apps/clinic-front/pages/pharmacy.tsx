import Navbar from "components/Navbar"
import { NextPage } from "next"
import { YMaps, Map, Placemark } from "react-yandex-maps";
import styled from "styled-components"

const Pharmacy = () => {
	const pharmacyData = [
		{
			name: 'Лекарство 1',
			inStock: true,
			price: '50$',
			code: '12312321'
		},
		{
			name: 'Лекарство 2',
			inStock: false,
			price: '20$',
			code: '12312321'
		},
		{
			name: 'Лекарство 3',
			inStock: true,
			price: '160$',
			code: '12312321'
		},
	]

	return (
		<>
			<Navbar />
			<div className="container mb-8">
				<PharmacyContainer className="rounded-xl p-8">
					<h1 className="text-xl font-bold mb-4">Лекарства</h1>
					<PharmacySearch />
					<div className="flex flex-wrap gap-12">
						{pharmacyData.map((item, index) => <PharmacyItem key={index} data={item} />)}
					</div>
					<PharmacyInfo />
				</PharmacyContainer>
			</div>
		</>
	)
}

const PharmacyItem: NextPage<{ data: { name: string, inStock: boolean, price: string, code: string } }> = (props) => {
	return (
		<div className="">
			<div className="text-lg font-semibold">{props.data.name}</div>
			<div className="font-medium">Номенклатурный код: {props.data.code}</div>
			<div className="font-medium">В наличии: {props.data.inStock ? 'да' : 'нет'}</div>
			<div className="font-medium">Цена: {props.data.price}</div>
		</div>
	)
}

const PharmacySearch = () => {
	return (
		<div className="my-6">
			<input
				type="text"
				placeholder="Название лекарства..."
				className="border-2 border-black rounded-lg px-2 h-9 w-1/4 outline-blue"
			/>
		</div>
	)
}

const PharmacyInfo = () => {
	return (
		<div className="mt-8">
			<YMaps>
				<div className="w-full rounded-l h-96">
					<Map
						state={{
							center: [52.2297700, 21.0117800],
							zoom: 18,
						}}
						width="100%"
						height="100%"
					>
						<Placemark
							geometry={[52.2297700, 21.0117800]}
						/>
					</Map>
				</div>
			</YMaps>
		</div>
	)
}

const PharmacyContainer = styled.div`
    /* margin-left: 10vw;
    margin-right: 10vw;
    margin-top: 46px;
    padding: 60px 100px; */
    background-color: #ffffff;
    color: black;
    border-radius: 20px;
    box-shadow: -10px 10px 20px 20px rgba(110, 1, 215, 0.1);
`;

export default Pharmacy