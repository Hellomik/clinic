import Navbar from "components/Navbar";
import PersonalCardTabs from "components/personal-card/PersonalCardTabs";
import styled from "styled-components";

const PersonalCard = () => {
	return (
		<div className="container mb-8">
			<Navbar />
			<PersonalCardContainer className="rounded-xl p-8">
				<PersonalCardTabs />
			</PersonalCardContainer>
		</div>
	)
}

const PersonalCardContainer = styled.div`
    /* margin-left: 10vw;
    margin-right: 10vw;
    margin-top: 46px;
    padding: 60px 100px; */
    background-color: #ffffff;
    color: black;
    border-radius: 20px;
    box-shadow: -10px 10px 20px 20px rgba(110, 1, 215, 0.1);
`;

export default PersonalCard