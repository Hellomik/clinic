import { gql } from "@apollo/client";

export const CREATE_BOOKING = gql`
    mutation CreateBooking(
        $doctorId: String!
        $endDate: DateTime!
        $serviceId: String!
        $startDate: DateTime!
        $userId: String!
    ) {
        createBooking(
            doctorId: $doctorId
            endDate: $endDate
            # serviceId: "63be83145df910fd3346c6d8"
            serviceId: $serviceId
            startDate: $startDate
            userId: $userId
        ) {
            _id
        }
    }
`;
