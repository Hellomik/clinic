import { NextPage } from "next"
import { useState } from "react"

const Labs = () => {
	const labsData = [
		{
			name: 'Лаборотория 1',
			services: ['Услуга 1', 'Услуга 2', 'Услуга 3', 'Услуга 4', 'Услуга 5',],
			price: '300$'
		},
		{
			name: 'Лаборотория 2',
			services: ['Услуга 1', 'Услуга 2', 'Услуга 3', 'Услуга 4', 'Услуга 5',],
			price: '100$'
		},
		{
			name: 'Лаборотория 3',
			services: ['Услуга 1', 'Услуга 2', 'Услуга 3', 'Услуга 4', 'Услуга 5',],
			price: '900$'
		}
	]

	return (
		<div className="">
			<h1 className="text-xl font-bold mb-4">Лаборотории</h1>
			<div className="flex flex-wrap gap-12">
				{labsData.map((item, index) => <LabsItem key={index} data={item} />)}
			</div>
			<div className="mt-8">
				<div className="text-xl font-semibold mb-3">Инструкции</div>
				<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius asperiores corporis quas nihil, quis, tempore nemo velit commodi odit aut cum et modi neque obcaecati. Necessitatibus vero officia quos?</div>
			</div>
			<div className="mt-8">
				<div className="text-xl font-semibold mb-3">Памятки</div>
				<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius asperiores corporis quas nihil, quis, tempore nemo velit commodi odit aut cum et modi neque obcaecati. Necessitatibus vero officia quos?</div>
			</div>
		</div>
	)
}

const LabsItem: NextPage<{ data: { name: string, services: string[], price: string } }> = (props) => {
	const [isActive, setIsActive] = useState(false)

	return (
		<div className="">
			<div className="text-lg font-semibold">{props.data.name}</div>
			<div className="my-2">
				<div className="font-semibold my-1">График</div>
				<div className="">
					<div className="">пон 10:00 - 16:00</div>
					<div className="">вт 8:00 - 12:00</div>
					<div className="">ср 10:00 - 18:00</div>
					<div className="">чт 8:00 - 16:00</div>
					<div className="">пн 10:00 - 16:00</div>
				</div>
			</div>
			<div className="">
				<div className="font-semibold my-1">Услуги:</div>
				{props.data.services.map((item, index) => <div key={index}>- {item}</div>)}
			</div>
			<div className="my-3">Цена услуг: {props.data.price}</div>
			<button className="bg-purple-400 rounded-lg w-32 text-white" onClick={() => setIsActive(!isActive)}>{!isActive ? 'Записаться' : 'Вы записаны'}</button>
		</div>
	)
}

export default Labs