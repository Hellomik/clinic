import { NextPage } from "next"
import { useState } from "react"

const Hospital = () => {
	const doctors = [
		{
			name: 'Рахат',
			surname: 'Баймухамедов',
			qualification: 'Терапевт'
		},
		{
			name: 'Бекзат',
			surname: 'Жарылкасын',
			qualification: 'Терапевт'
		},
		{
			name: 'Валерий',
			surname: 'Чупик',
			qualification: 'Терапевт'
		},
	]

	return (
		<div className="">
			<div className="text-xl font-semibold mb-3">Врачи</div>
			<div className="flex flex-wrap gap-10">
				{doctors.map((d, index) => <DoctorItem key={index} data={d} />)}
			</div>
			<div className="mt-8">
				<div className="text-xl font-semibold mb-3">Инструкции</div>
				<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius asperiores corporis quas nihil, quis, tempore nemo velit commodi odit aut cum et modi neque obcaecati. Necessitatibus vero officia quos?</div>
			</div>
			<div className="mt-8">
				<div className="text-xl font-semibold mb-3">Памятки</div>
				<div className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius asperiores corporis quas nihil, quis, tempore nemo velit commodi odit aut cum et modi neque obcaecati. Necessitatibus vero officia quos?</div>
			</div>
			<div className="mt-8">
				<div className="text-xl font-semibold mb-3">Прикрепленные участки</div>
				<div className="">280 Yukon Ave.Syosset, NY 11791</div>
				<div className="">67 Pulaski Dr.Hattiesburg, MS 39401</div>
				<div className="">46 S. Bow Ridge Ave.Flowery Branch, GA 30542</div>
			</div>
		</div>
	)
}

const DoctorItem: NextPage<{ data: { name: string, surname: string, qualification: string } }> = (props) => {
	const [isActive, setIsActive] = useState(false)

	return (
		<div className="">
			<div className="flex gap-4">
				<div className="w-16 h-16 bg-purple-700 rounded-full"></div>
				<div className="">
					<div className="font-semibold">{props.data.name}</div>
					<div className="font-semibold">{props.data.surname}</div>
					<div className="">{props.data.qualification}</div>
				</div>
			</div>
			<div className="my-2">
				<div className="font-semibold my-2">График</div>
				<div className="">
					<div className="">пон 10:00 - 16:00</div>
					<div className="">вт 8:00 - 12:00</div>
					<div className="">ср 10:00 - 18:00</div>
					<div className="">чт 8:00 - 16:00</div>
					<div className="">пн 10:00 - 16:00</div>
				</div>
			</div>
			<button className="bg-purple-400 rounded-lg w-32 text-white" onClick={() => setIsActive(!isActive)}>{!isActive ? 'Направить' : 'Направлен'}</button>
		</div>
	)
}

export default Hospital