import React, { useEffect } from "react";

import { useGetAllUsersQuery } from "@lucem/shared-gql";
import { Center, Container, Spinner } from "@chakra-ui/react";
import PatientsPageItem from "components/Patients/PatientItem";

export const testPatientsData = [
    {
        id: 1,
        image: null as string | null,
        name: 'Рахат',
        surname: 'Баймухамедов',
        IIN: '000115781332',
        address: 'г Алматы, улица Жуманова 46',
        telNumber: '87759551015',
        dateOfBirth: '2000-01-16',
        email: 'textuser@gmail.com',
        analyzesResult: {
            therapist: 'Сайфуула Ахмедов',
            diagnosis: 'Болезнь Крона',
            conclusion: 'Остарый трансмуральный инфаркт миокадра (тип 1) передней и боковой стенок левого желудочка (давность около 3 суток, размеры очага некроза)'
        },
        docrecommendaions: null,
        dispensaryRegistration: null,
        observationDiary: [
            {
                date: '03.01.2022',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem sapiente eius magni animi inventore. Sunt quaerat hic voluptatibus accusantium illum asperiores, quidem dignissimos officia vel optio beatae placeat adipisci incidunt!"
            },
            {
                date: '24.11.2022',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem sapiente eius magni animi inventore. Sunt quaerat hic voluptatibus accusantium illum asperiores."
            },
            {
                date: '02.01.2023',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit."
            }
        ],
        hospitalDocuments: [
            {
                title: 'Больничный лист 1',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius, culpa nulla consectetur temporibus quibusdam alias incidunt tenetur laboriosam placeat modi. Inventore magnam cupiditate veniam ipsam expedita, ea ut nobis eveniet?"
            },
            {
                title: 'Больничный лист 2',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro harum doloremque quae ea nesciunt aperiam suscipit consequatur sapiente. Earum eum corporis, repellat dolorum dolores ipsum harum fuga nemo totam velit."
            }
        ],
        dischargeEpicrisis: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor impedit optio corporis officiis alias, delectus obcaecati sapiente porro quas vel. Odit, eligendi quos possimus debitis eius laborum fugit voluptates exercitationem.",
        milestoneEpicrisis: null,
        recipes: [],
        referralsForTests: [],
        callHistory: [
            {
                dateAndTime: '06.12.2022, 10:45 - 11:00',
                reasonForCalling: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, harum?",
                anamnesis: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sit laboriosam ullam nostrum consequatur dignissimos!",
                primaryDiagnosis: "Lorem, ipsum.",
                procedures: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
                destination: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime eveniet corrupti in debitis deserunt nemo similique aliquam dolore repudiandae omnis. Modi optio quasi facilis corporis earum omnis eum incidunt repellendus?",
                hospitalization: true
            },
            {
                dateAndTime: '03.01.2023, 12:47 - 13:23',
                reasonForCalling: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, harum?",
                anamnesis: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sit laboriosam ullam nostrum consequatur dignissimos!",
                primaryDiagnosis: "Lorem, ipsum.",
                procedures: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
                destination: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime eveniet corrupti in debitis deserunt nemo similique aliquam dolore repudiandae omnis. Modi optio quasi facilis corporis earum omnis eum incidunt repellendus?",
                hospitalization: false
            },
        ],
        aboutFamily: null
    },

    {
        id: 2,
        image: null as string | null,
        name: 'Бекзат',
        surname: 'Жарылкасын',
        IIN: '546115787792',
        address: 'г Алматы, улица Абоя 98',
        telNumber: '87754679190',
        dateOfBirth: '1996-02-28',
        email: 'textuser2@gmail.com',
        analyzesResult: {
            therapist: 'Сайфуула Ахмедов',
            diagnosis: 'Желчнокаменная болезнь',
            conclusion: 'Ишемический инфаркт лобной доли правого полушария головного мозга (атеротромбитический, размеры очага некроза)'
        },
        docrecommendaions: null,
        dispensaryRegistration: null,
        observationDiary: [
            {
                date: '03.01.2022',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem sapiente eius magni animi inventore. Sunt quaerat hic voluptatibus accusantium illum asperiores, quidem dignissimos officia vel optio beatae placeat adipisci incidunt!"
            },
            {
                date: '24.11.2022',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem sapiente eius magni animi inventore. Sunt quaerat hic voluptatibus accusantium illum asperiores."
            },
            {
                date: '02.01.2023',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit."
            }
        ],
        hospitalDocuments: [
            {
                title: 'Больничный лист 1',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius, culpa nulla consectetur temporibus quibusdam alias incidunt tenetur laboriosam placeat modi. Inventore magnam cupiditate veniam ipsam expedita, ea ut nobis eveniet?"
            },
            {
                title: 'Больничный лист 2',
                text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro harum doloremque quae ea nesciunt aperiam suscipit consequatur sapiente. Earum eum corporis, repellat dolorum dolores ipsum harum fuga nemo totam velit."
            }
        ],
        dischargeEpicrisis: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor impedit optio corporis officiis alias, delectus obcaecati sapiente porro quas vel. Odit, eligendi quos possimus debitis eius laborum fugit voluptates exercitationem.",
        milestoneEpicrisis: null,
        recipes: [],
        referralsForTests: [],
        callHistory: [
            {
                dateAndTime: '06.12.2022, 10:45 - 11:00',
                reasonForCalling: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, harum?",
                anamnesis: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sit laboriosam ullam nostrum consequatur dignissimos!",
                primaryDiagnosis: "Lorem, ipsum.",
                procedures: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
                destination: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime eveniet corrupti in debitis deserunt nemo similique aliquam dolore repudiandae omnis. Modi optio quasi facilis corporis earum omnis eum incidunt repellendus?",
                hospitalization: true
            },
            {
                dateAndTime: '03.01.2023, 12:47 - 13:23',
                reasonForCalling: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, harum?",
                anamnesis: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sit laboriosam ullam nostrum consequatur dignissimos!",
                primaryDiagnosis: "Lorem, ipsum.",
                procedures: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
                destination: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime eveniet corrupti in debitis deserunt nemo similique aliquam dolore repudiandae omnis. Modi optio quasi facilis corporis earum omnis eum incidunt repellendus?",
                hospitalization: false
            },
        ],
        aboutFamily: null
    }
]

const PatientsPage: React.FC = () => {
    const { data, loading } = useGetAllUsersQuery();
    if (loading) {
        return (
            <Container>
                <Center>
                    <Spinner></Spinner>
                </Center>
            </Container>
        );
    }

    return (
        <div className="flex gap-8">
            {testPatientsData.map((item) => <PatientsPageItem key={item.id} data={item} />)}
        </div>
    );
};

export default PatientsPage;
