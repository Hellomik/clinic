import { NextPage } from "next"
import { useState } from "react"
import { ModalContainer } from "../PatientItem"

const CallHistory: NextPage<{ callHistory: any }> = (props) => {
	const [callHistoryData, setCallHistoryData] = useState(props.callHistory)

	return (
		<div className="">
			<h2 className="text-xl my-3">Истории вызовов СМП</h2>
			<div className="flex flex-col gap-4">
				{callHistoryData.map((item, index) => <CallHistoryItem key={index} item={item} />)}
			</div>
		</div>
	)
}

const CallHistoryItem: NextPage<{ item: any }> = (props) => {
	const [isModal, setIsModal] = useState(false)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="shadow-first p-4 rounded-lg">
			{isModal &&
				<CallHistoryItemModal closeModal={closeModal} />
			}
			<div className="font-semibold mb-3">{props.item.dateAndTime}</div>
			<div className="flex flex-col gap-3">
				<div className="flex flex-col gap-1"><span className="font-semibold">Причина вызова:</span> {props.item.reasonForCalling}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Анамнез:</span> {props.item.anamnesis}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Первичный диагноз:</span> {props.item.primaryDiagnosis}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Процедуры:</span> {props.item.procedures}</div>
				<div className="flex flex-col gap-1"><span className="font-semibold">Назначения:</span> {props.item.destination}</div>
				<div className=""><span className="font-semibold">Госпитализация:</span> {props.item.hospitalization ? 'Да' : 'Нет'}</div>
			</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-2 ml-auto block" onClick={openModal}>Редактировать</button>
		</div>
	)
}

const CallHistoryItemModal: NextPage<{ closeModal: () => void }> = (props) => {
	return (
		<ModalContainer 
			title="Редактирование"
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Сохранить',
				executeButtonHandler() {
					
				},
			}}
		>

		</ModalContainer>
	)
}

export default CallHistory