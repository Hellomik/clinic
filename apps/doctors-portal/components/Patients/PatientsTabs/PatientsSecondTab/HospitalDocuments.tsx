import { ModalContainer } from "components/Patients/PatientItem"
import { NextPage } from "next"
import { useRef, useState } from "react"

interface IHospitalDocuments {
	hospitalDocuments: Array<{ title: string, text: string }>
}

const HospitalDocuments: NextPage<IHospitalDocuments> = (props) => {
	const [isModal, setIsModal] = useState(false)
	const [hospitalDocumentsData, setHospitalDocumentsData] = useState(props.hospitalDocuments)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="">
			{isModal &&
				<HospitalDocumentsCreateModal closeModal={closeModal} setData={setHospitalDocumentsData} />
			}
			<h2 className="text-xl my-3">Больничные листы</h2>
			<div className="flex flex-col gap-4">
				{hospitalDocumentsData.map((item, index) => <HospitalDocumentsItem key={index} item={item} />)}
			</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4" onClick={openModal}>Создать больничный лист</button>
		</div>
	)
}

const HospitalDocumentsItem: NextPage<{ item: { title: string, text: string } }> = (props) => {
	return (
		<div className="p-4 shadow-first rounded-lg">
			<div className="text-lg mb-3">{props.item.title}</div>
			<div className="">{props.item.text}</div>
		</div>
	)
}

interface IHospitalDocumentsCreateModal {
	closeModal: () => void
	setData: (data: any) => void
}

const HospitalDocumentsCreateModal: NextPage<IHospitalDocumentsCreateModal> = (props) => {
	const [titleText, setTitleText] = useState('')
	const [mainText, setMainText] = useState('')
	const titleRef = useRef(null)
	const mainTextRef = useRef(null)

	const addItem = () => {
		if (!titleText) {
			titleRef.current.focus()
			return
		}
		if (!mainText) {
			mainTextRef.current.focus()
			return
		}
		props.setData(prevData => {
			return [...prevData, {
				title: titleText,
				text: mainText
			}]
		})
		props.closeModal()
	}

	return (
		<ModalContainer
			title="Создание"
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Создать',
				executeButtonHandler: addItem
			}}
		>
			<div className="flex flex-col gap-3">
				<input
					type="text"
					placeholder="Название листа..."
					className="border-1 border-black rounded-md px-2 h-9 outline-none"
					value={titleText}
					onChange={(e) => setTitleText(e.currentTarget.value)}
					ref={titleRef}
				/>
				<textarea
					placeholder='Текст листа...'
					className="p-2 resize-none w-full border-1 border-black rounded-md h-44 outline-none"
					value={mainText}
					onChange={(e) => setMainText(e.currentTarget.value)}
					ref={mainTextRef}
				/>
			</div>
		</ModalContainer>
	)
}

export default HospitalDocuments