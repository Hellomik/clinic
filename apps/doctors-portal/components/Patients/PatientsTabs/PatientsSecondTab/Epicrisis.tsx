import { ModalContainer } from "components/Patients/PatientItem"
import { NextPage } from "next"
import { useEffect, useRef, useState } from "react"

export const DischargeEpicrisis: NextPage<{ dischargeEpicrisisText: string | null }> = (props) => {
	const [text, setText] = useState(!props.dischargeEpicrisisText ? '' : props.dischargeEpicrisisText)
	const [isModal, setIsModal] = useState(false)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="">
			{isModal &&
				<DischargeEpicrisisEditModal closeModal={closeModal} text={text} setText={setText} />
			}
			<h2 className="text-xl my-3">Выписной эпикриз</h2>
			<div className="">{!text ? 'Пусто' : text}</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4" onClick={openModal}>Добавить</button>
		</div>
	)
}

interface IDischargeEpicrisisEditModal {
	closeModal: () => void
	setText: (data: any) => void
	text: string
}

const DischargeEpicrisisEditModal: NextPage<IDischargeEpicrisisEditModal> = (props) => {
	const [text, setText] = useState(props.text)
	const inputRef = useRef(null)

	useEffect(() => {
		inputRef.current.focus()
		inputRef.current.scrollTop = inputRef.current.scrollHeight
	}, [])

	const addItem = () => {
		props.setText(text)
		props.closeModal()
	}

	return (
		<ModalContainer
			title="Редактирование"
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Сохранить',
				executeButtonHandler: addItem
			}}
		>
			<textarea
				className="p-2 resize-none w-full border-1 border-black rounded-md h-44 outline-none"
				value={text}
				onChange={(e) => setText(e.currentTarget.value)}
				placeholder='Текст...'
				ref={inputRef}
				onFocus={(e) => e.currentTarget.setSelectionRange(e.currentTarget.value.length, e.currentTarget.value.length)}
			/>
		</ModalContainer>
	)
}

export const MilestoneEpicrisis: NextPage<{ milestoneEpicrisisText: string | null }> = (props) => {
	const [text, setText] = useState(!props.milestoneEpicrisisText ? '' : props.milestoneEpicrisisText)
	const [isModal, setIsModal] = useState(false)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="">
			{isModal &&
				<MilestoneEpicrisisEditModal closeModal={closeModal} setText={setText} text={text} />
			}
			<h2 className="text-xl my-3">Этапный эпикриз</h2>
			<div className="">{!text ? 'Пусто' : text}</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4" onClick={openModal}>Добавить</button>
		</div>
	)
}

interface IMilestoneEpicrisisEditModal {
	closeModal: () => void
	setText: (data: any) => void
	text: string
}

const MilestoneEpicrisisEditModal: NextPage<IMilestoneEpicrisisEditModal> = (props) => {
	const [text, setText] = useState(props.text)
	const inputRef = useRef(null)

	useEffect(() => {
		inputRef.current.focus()
		inputRef.current.scrollTop = inputRef.current.scrollHeight
	}, [])

	const addItem = () => {
		props.setText(text)
		props.closeModal()
	}

	return (
		<ModalContainer
			title="Редактирование"
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Сохранить',
				executeButtonHandler: addItem
			}}
		>
			<textarea
				className="p-2 resize-none w-full border-1 border-black rounded-md h-44 outline-none"
				value={text}
				onChange={(e) => setText(e.currentTarget.value)}
				placeholder='Текст...'
				ref={inputRef}
				onFocus={(e) => e.currentTarget.setSelectionRange(e.currentTarget.value.length, e.currentTarget.value.length)}
			/>
		</ModalContainer>
	)
}
