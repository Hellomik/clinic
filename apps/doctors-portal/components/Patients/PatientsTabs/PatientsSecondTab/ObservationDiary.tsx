import { ModalContainer } from "components/Patients/PatientItem";
import { NextPage } from "next";
import { useRef, useState } from "react";

interface IObservationDiary {
	observationDiary: Array<{ date: string, text: string }>
}

export const ObservationDiary: NextPage<IObservationDiary> = (props) => {
	const [isModal, setIsModal] = useState(false)
	const [observationDiaryData, setObservationDiaryData] = useState(props.observationDiary)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="">
			{isModal &&
				<ObservationDiaryModal setData={setObservationDiaryData} closeModal={closeModal} />
			}
			<h2 className="text-xl my-3">Дневник наблюдений</h2>
			<div className="max-h-300 overflow-y-auto">
				<div className="border-1 border-black rounded-sm">
					{observationDiaryData.map((item, index) => {
						return (
							<div key={index} className={`flex ${index !== 0 ? 'border-t-1 border-black' : ''}`}>
								<div className="p-2 border-r-1 border-black min-w-20 md:min-w-30 flex justify-center items-center">{item.date}</div>
								<div className="p-2">{item.text}</div>
							</div>
						);
					})}
				</div>
			</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4" onClick={openModal}>Добавить запись</button>
		</div>
	);
};

interface IObservationDiaryModal {
	closeModal: () => void
	setData: (data: any) => void
}

const ObservationDiaryModal: NextPage<IObservationDiaryModal> = (props) => {
	const [text, setText] = useState('')
	const inputRef = useRef(null)

	const addItem = () => {
		if (!text) {
			inputRef.current.focus()
		} else {
			props.setData((prev) => {
				return [...prev, {
					date: new Date().toLocaleDateString(),
					text
				}]
			})
			props.closeModal()
		}
	}

	return (
		<ModalContainer 
			title="Добавление" 
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Добавить',
				executeButtonHandler: addItem
			}}
		>
			<textarea
				className="p-2 resize-none w-full border-1 border-black rounded-md h-44 outline-none"
				value={text}
				onChange={(e) => setText(e.currentTarget.value)}
				placeholder='Текст записи...'
				ref={inputRef}
			/>
		</ModalContainer>
	)
}