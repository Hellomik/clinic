import { NextPage } from "next"

const Recipes: NextPage<{ recipes: string[] }> = (props) => {
	return (
		<div className="">
			<h2 className="text-lg mb-3 font-semibold">Рецепты</h2>
			<div className="">
				{props.recipes.length < 1 
					? 'Пока что нет рецептов' 
					: props.recipes.map((item, index) => <div key={index}>{item}</div>)
				}
			</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4">Добавить рецепт</button>
		</div>
	)
}

export default Recipes