import { NextPage } from "next"
import Image from "next/image"
import { useRouter } from "next/router"
import { testPatientsData } from "pages/patients"
import { useState, useRef, useEffect } from "react"
import AboutFamily from "../AboutFamily"
import DispensaryRegistration from "./DispensaryRegistration"
import { PatientPageDocRecommendations } from "./PatientPageDocRecommendations"
import ReferralsForTests from "./ReferralsForTests"

interface IPatientPage {
	userInfo: typeof testPatientsData[0]
	setUserInfo: (info: any) => void
}

export const PatientPage: NextPage<IPatientPage> = (props) => {
	const router = useRouter()
	const [isEditDiagnosis, setIsEditDiagnosis] = useState(false)
	const [isEditConclusion, setIsEditConclusion] = useState(false)
	const diagnosisRef = useRef(null)
	const conclusionRef = useRef(null)

	const setDiagnosis = () => {
		props.setUserInfo({
			...props.userInfo,
			analyzesResult: {
				...props.userInfo.analyzesResult,
				diagnosis: diagnosisRef.current.value,
			}
		})
		testPatientsData.find(i => i.id === Number(router.query.patientId)).analyzesResult.diagnosis = diagnosisRef.current.value
		setIsEditDiagnosis(false)
	}

	const setConclusion = () => {
		props.setUserInfo({
			...props.userInfo,
			analyzesResult: {
				...props.userInfo.analyzesResult,
				conclusion: conclusionRef.current.value,
			}
		})
		testPatientsData.find(i => i.id === Number(router.query.patientId)).analyzesResult.conclusion = conclusionRef.current.value
		setIsEditConclusion(false)
	}

	return (
		<div className="">
			<PatientPageHeader userInfo={props.userInfo} />
			<PatientPageInfo userInfo={props.userInfo} />
			<div className="">
				<div className="mb-1 font-[20px] font-semibold text-xl mt-6">Результаты анализов</div>
				<div className="flex flex-col gap-4">
					<div className="">
						<div className="font-semibold">Терапевт:</div>
						<div className="">{props.userInfo.analyzesResult.therapist}</div>
					</div>
					<div className="">
						<div className="font-semibold">Диагноз:</div>
						{!isEditDiagnosis
							? <div className="">
								<div>{props.userInfo.analyzesResult.diagnosis}</div>
								<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-2" onClick={() => setIsEditDiagnosis(true)}>Изменить</button>
							</div>
							: <div className="">
								<input className="px-2 block resize-none border-1 border-black rounded-md outline-none" defaultValue={props.userInfo.analyzesResult.diagnosis} type="text" ref={diagnosisRef} />
								<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-2" onClick={setDiagnosis}>Сохранить</button>
							</div>
						}
					</div>
					<div className="">
						<div className="font-semibold">Заключение:</div>
						{!isEditConclusion
							? <div className="">
								<div>{props.userInfo.analyzesResult.conclusion}</div>
								<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-2" onClick={() => setIsEditConclusion(true)}>Изменить</button>
							</div>
							: <div className="">
								<textarea className="p-2 resize-none w-full border-1 border-black rounded-md h-24 outline-none" defaultValue={props.userInfo.analyzesResult.conclusion} ref={conclusionRef} />
								<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-2" onClick={setConclusion}>Сохранить</button>
							</div>
						}
					</div>
				</div>
			</div>
			<div className="">
				<PatientPageDocRecommendations docRecommendations={props.userInfo.docrecommendaions} />
				<DispensaryRegistration dispensaryRegistration={props.userInfo.dispensaryRegistration} />
				<ReferralsForTests referralsForTests={props.userInfo.referralsForTests} />
				<AboutFamily aboutFamily={props.userInfo.aboutFamily} />
			</div>
		</div>
	)
}

const PatientPageHeader: NextPage<{ userInfo: typeof testPatientsData[0] }> = (props) => {
	return (
		<div className="flex gap-4">
			<div className="">
				{props.userInfo.image
					? <Image src={props.userInfo.image} alt="" />
					: <div className="w-20 h-20 bg-purple-700 rounded-full"></div>}
			</div>
			<div className="">
				<div className="text-xl font-semibold">{props.userInfo.name}</div>
				<div className="text-xl font-semibold">{props.userInfo.surname}</div>
				<div className="">ИИН: {props.userInfo.IIN}</div>
			</div>
		</div>
	)
}


const PatientPageInfo: NextPage<{ userInfo: typeof testPatientsData[0] }> = (props) => {
	return (
		<div className="mt-3">
			<div className="font-bold text-lg mb-1">Данные пациента</div>
			<div className="flex gap-10">
				<div>
					<div className="">
						<div className="font-bold font-[18px]">Адрес:</div>
						<span>{props.userInfo.address}</span>
					</div>
					<div className="mt-5">
						<div className="font-bold">Дата рождения:</div>
						<span>{props.userInfo.dateOfBirth}</span>
					</div>
				</div>
				<div>
					<div className="">
						<div className="font-bold">Номер телефона:</div>
						<span>{props.userInfo.telNumber}</span>
					</div>
					<div className="mt-5">
						<div className="font-bold">Электронная почта:</div>
						<span>{props.userInfo.email}</span>
					</div>
				</div>
			</div>
		</div>
	)
}


