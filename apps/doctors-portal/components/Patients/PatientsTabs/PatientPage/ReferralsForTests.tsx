import { NextPage } from "next"

const ReferralsForTests: NextPage<{ referralsForTests: string[] }> = (props) => {
	return (
		<div className="border-t-1 border-gray-500 mt-5 pt-3">
			<h2 className="text-lg mb-3 font-semibold">Направления на анализы</h2>
			<div className="">
				{props.referralsForTests.length < 1 
					? 'Пока что нет направлений' 
					: props.referralsForTests.map((item, index) => <div key={index}>{item}</div>)
				}
			</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4">Добавить направление</button>
		</div>
	)
}

export default ReferralsForTests