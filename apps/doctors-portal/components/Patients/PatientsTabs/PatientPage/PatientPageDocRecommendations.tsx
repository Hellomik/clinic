import { ModalContainer } from "components/Patients/PatientItem";
import { NextPage } from "next";
import { useRef, useState } from "react";

export const PatientPageDocRecommendations: NextPage<{ docRecommendations: string | null; }> = (props) => {
	const [isModal, setIsModal] = useState(false)
	const [docRecommendations, setDocRecommendations] = useState<string | null>(props.docRecommendations)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="border-t-1 border-gray-500 mt-5 pt-3">
			{isModal &&
				<PatientPageDocRecommendationsModal closeModal={closeModal} setData={setDocRecommendations} data={docRecommendations} />
			}
			<h2 className="text-lg mb-3 font-semibold">Рекомендации врача</h2>
			<div className="">{!docRecommendations ? 'Пусто' : docRecommendations}</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4" onClick={openModal}>
				{!docRecommendations
					? 'Добавить'
					: 'Редактировать'
				}
			</button>
		</div>
	);
};

interface IPatientPageDocRecommendationsModal {
	closeModal: () => void
	data: string | null
	setData: (data: any) => void
}

const PatientPageDocRecommendationsModal: NextPage<IPatientPageDocRecommendationsModal> = (props) => {
	const inputRef = useRef(null)
	const [text, setText] = useState(!props.data ? '' : props.data)

	const setRecommendations = () => {
		if (!text) {
			inputRef.current.focus()
		} else {
			props.setData(text)
			props.closeModal()
		}
	}

	return (
		<ModalContainer 
			title={!props.data ? 'Добавление' : 'Редактирование'} 
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Сохранить',
				executeButtonHandler: setRecommendations
			}}
		>
			<textarea
				className="p-2 resize-none w-full border-1 border-black rounded-md h-44 outline-none"
				value={text}
				onChange={(e) => setText(e.currentTarget.value)}
				placeholder='Рекомендации...'
				ref={inputRef}
			/>
		</ModalContainer>
	)
}