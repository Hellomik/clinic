import { ModalContainer } from "components/Patients/PatientItem"
import { NextPage } from "next"
import { useRef, useState } from "react"

const DispensaryRegistration: NextPage<{ dispensaryRegistration: string | null }> = (props) => {
	const [isModal, setIsModal] = useState(false)
	const [dispensaryRegistration, setDispensaryRegistration] = useState<string | null>(props.dispensaryRegistration)

	const openModal = () => {
		setIsModal(true)
		document.body.style.overflowY = 'hidden'
	}

	const closeModal = () => {
		setIsModal(false)
		document.body.style.overflowY = 'auto'
	}

	return (
		<div className="border-t-1 border-gray-500 mt-5 pt-3">
			{isModal &&
				<DispensaryRegistrationModal closeModal={closeModal} setData={setDispensaryRegistration} data={dispensaryRegistration} />
			}
			<h2 className="text-lg mb-3 font-semibold">Диспансерный учет</h2>
			<div className="">{!dispensaryRegistration ? 'Пусто' : dispensaryRegistration}</div>
			<button className="bg-pink-purple text-sm py-1 px-2 rounded-lg text-white mt-4" onClick={openModal}>
				{!dispensaryRegistration
					? 'Добавить'
					: 'Редактировать'
				}
			</button>
		</div>
	)
}

interface IDispensaryRegistrationModal {
	closeModal: () => void
	data: string | null
	setData: (data: any) => void
}

const DispensaryRegistrationModal: NextPage<IDispensaryRegistrationModal> = (props) => {
	const inputRef = useRef(null)
	const [text, setText] = useState(!props.data ? '' : props.data)

	const setRecommendations = () => {
		if (!text) {
			inputRef.current.focus()
		} else {
			props.setData(text)
			props.closeModal()
		}
	}

	return (
		<ModalContainer 
			title={!props.data ? 'Добавление' : 'Редактирование'} 
			buttons={{
				closeButtonName: 'Отмена',
				closeButtonHandler: props.closeModal,
				executeButtonName: 'Сохранить',
				executeButtonHandler: setRecommendations
			}}
		>
			<textarea
				className="p-2 resize-none w-full border-1 border-black rounded-md h-44 outline-none"
				value={text}
				onChange={(e) => setText(e.currentTarget.value)}
				placeholder='Текст...'
				ref={inputRef}
			/>
		</ModalContainer>
	)
}

export default DispensaryRegistration