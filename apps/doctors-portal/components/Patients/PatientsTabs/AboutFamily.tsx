import { NextPage } from "next"


const AboutFamily: NextPage<{ aboutFamily: string | null }> = (props) => {
	return (
		<div className="border-t-1 border-gray-500 mt-5 pt-3">
			<h2 className="text-lg mb-3 font-semibold">Сведения о семье</h2>
			<div className="">{!props.aboutFamily ? 'Пусто': props.aboutFamily}</div>
		</div>
	)
}

export default AboutFamily