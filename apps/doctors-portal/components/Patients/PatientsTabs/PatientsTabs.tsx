import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@chakra-ui/react"
import { DischargeEpicrisis, MilestoneEpicrisis } from "./PatientsSecondTab/Epicrisis"
import HospitalDocuments from "./PatientsSecondTab/HospitalDocuments"
import { ObservationDiary } from "./PatientsSecondTab/ObservationDiary"
import { PatientPage } from "./PatientPage/PatientPage"
import { testPatientsData } from "pages/patients"
import { useEffect, useState } from "react"
import { useRouter } from "next/router"
import Recipes from "./Recipes"
import CallHistory from "./CallHistory"

const PatientsTabs = () => {
	const router = useRouter()
	const [userInfo, setUserInfo] = useState<typeof testPatientsData[0] | null>(null)

	useEffect(() => {
		setUserInfo(testPatientsData.find(i => i.id === Number(router.query.patientId)))
	}, [])

	if (!userInfo) {
		return <div className="">Laoding...</div>
	}
	return (
		<div className="">
			<Tabs>
				<TabList
					sx={{
						overflowX: 'auto',
						overflowY: 'hidden'
					}}
				>
					<Tab>Личная информация</Tab>
					<Tab>Стационар</Tab>
					<Tab>Календарь беременности</Tab>
					<Tab>Истории вызовов СМП</Tab>
					<Tab>Рецепты</Tab>
					<Tab>Документы</Tab>
				</TabList>
				<TabPanels>
					<TabPanel>
						<PatientPage userInfo={userInfo} setUserInfo={setUserInfo} />
					</TabPanel>
					<TabPanel className="flex flex-col gap-6">
						<ObservationDiary observationDiary={userInfo.observationDiary} />
						<HospitalDocuments hospitalDocuments={userInfo.hospitalDocuments} />
						<DischargeEpicrisis dischargeEpicrisisText={userInfo.dischargeEpicrisis} />
						<MilestoneEpicrisis milestoneEpicrisisText={userInfo.milestoneEpicrisis} />
					</TabPanel>
					<TabPanel>
						Календарь беременности
					</TabPanel>
					<TabPanel>
						<CallHistory callHistory={userInfo.callHistory} />
					</TabPanel>
					<TabPanel>
						<Recipes recipes={userInfo.recipes} />
					</TabPanel>
					<TabPanel>
						Документы
					</TabPanel>
				</TabPanels>
			</Tabs>
		</div>
	)
}


export default PatientsTabs