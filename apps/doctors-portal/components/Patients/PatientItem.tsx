import { PDFDownloadLink } from "@react-pdf/renderer"
import { NextPage } from "next"
import Image from "next/image"
import Link from "next/link"
import { testPatientsData } from "pages/patients"
import PDFfile from "./PDFfile"

const PatientsPageItem: NextPage<{ data: typeof testPatientsData[0] }> = (props) => {
	return (
		<div className="p-4 w-xxl shadow-first rounded-xl">
			<PatientsPageItemHeader data={props.data} />
			<PatientsPageItemHeaderAboutClient data={props.data} />
			<div className="">
				<div className="mb-1 font-[20px] font-semibold text-xl mt-6">Результаты анализов</div>
				<div className="flex flex-col gap-4">
					<div className="">
						<div className="font-semibold">Терапевт:</div>
						<div className="">{props.data.analyzesResult.therapist}</div>
					</div>
					<div className="">
						<div className="font-semibold">Диагноз:</div>
						<div className="">{props.data.analyzesResult.diagnosis}</div>
					</div>
					<div className="">
						<div className="font-semibold">Заключение:</div>
						<div className="">{props.data.analyzesResult.conclusion}</div>
					</div>
					{/* <div className="">
						<div className="font-semibold">Рецепт:</div>
						<div className="">{props.data.recipe}</div>
					</div>
					<div className="">
						<div className="font-semibold">Назначения:</div>
						<div className="">{props.data.appointments}</div>
					</div> */}
				</div>
			</div>
			<PDFDownloadLink document={<PDFfile {...props.data.analyzesResult} />} fileName="info" >
				{({ loading }) => {
					return !loading
						? <button className="border-2 border-black mx-auto mt-4 w-52 h-8 flex justify-center items-center rounded-xl">Скачать анализ</button>
						: <button className="border-2 border-black mx-auto mt-4 w-52 h-8 flex justify-center items-center rounded-xl">Загрузка файла</button>
				}}
			</PDFDownloadLink>
		</div>
	)
}

const PatientsPageItemHeader: NextPage<{ data: typeof testPatientsData[0] }> = (props) => {
	return (
		<Link href={`patients/${props.data.id}`}>
			<div className="flex gap-4 cursor-pointer">
				<div className="">
					{props.data.image
						? <Image src={props.data.image} alt="" className="w-20 h-20 rounded-full" />
						: <div className="w-20 h-20 bg-purple-700 rounded-full"></div>
					}
				</div>
				<div className="">
					<div className="text-xl font-semibold">{props.data.name}</div>
					<div className="text-xl font-semibold">{props.data.surname}</div>
					<div className="">ИИН: {props.data.IIN}</div>
				</div>
			</div>
		</Link>

	)
}

const PatientsPageItemHeaderAboutClient: NextPage<{ data: typeof testPatientsData[0] }> = (props) => {
	return (
		<div className="mt-3">
			<div className="font-bold text-lg mb-1">Данные пациента</div>
			<div className="flex gap-10">
				<div>
					<div className="">
						<div className="font-bold font-[18px]">Адрес:</div>
						<span>{props.data.address}</span>
					</div>
					<div className="mt-5">
						<div className="font-bold">Дата рождения:</div>
						<span>{props.data.dateOfBirth}</span>
					</div>
				</div>
				<div>
					<div className="">
						<div className="font-bold">Номер телефона:</div>
						<span>{props.data.telNumber}</span>
					</div>
					<div className="mt-5">
						<div className="font-bold">Электронная почта:</div>
						<span>{props.data.email}</span>
					</div>
				</div>
			</div>
		</div>
	)
}

interface IModalConatiner {
	children?: any
	title: string
	buttons: {
		closeButtonName: string
		closeButtonHandler: () => void
		executeButtonName: string
		executeButtonHandler: () => void
	}
}

export const ModalContainer: NextPage<IModalConatiner> = (props) => {
	return (
		<div className="fixed w-full h-full top-0 left-0 bg-modal-black flex justify-center items-center">
			<div className="bg-white p-6 rounded-xl">
				<h2 className="text-xl mb-4">{props.title}</h2>
				{props.children}
				<div className="flex gap-2 mt-4">
					<button className="bg-pink-purple rounded-lg w-32 text-white block" onClick={props.buttons.closeButtonHandler}>{props.buttons.closeButtonName}</button>
					<button className="bg-pink-purple rounded-lg w-32 text-white block" onClick={props.buttons.executeButtonHandler}>{props.buttons.executeButtonName}</button>
				</div>
			</div>
		</div>
	)
}

export default PatientsPageItem