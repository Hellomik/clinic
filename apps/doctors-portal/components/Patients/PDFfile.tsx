import { Page, Text, Image, Document, StyleSheet, Font, } from "@react-pdf/renderer";
import { NextPage } from "next";

Font.register({
	family: "Roboto",
	src: "https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-medium-webfont.ttf"
});

const styles = StyleSheet.create({
	body: {
		padding: 20,
		fontFamily: 'Roboto'
	},
	text: {
		fontSize: '14px'
	}
})

interface IPDFfile {
	therapist: string
	diagnosis: string
	conclusion: string
}

const PDFfile: NextPage<IPDFfile> = (props) => {
	return (
		<Document>
			<Page style={styles.body}>
				<div style={{ marginBottom: '20px' }}>
					<Text>Терапевт:</Text>
					<Text style={styles.text}>{props.therapist}</Text>
				</div>
				<div style={{ marginBottom: '20px' }}>
					<Text>Диагноз:</Text>
					<Text style={styles.text}>{props.diagnosis}</Text>
				</div>
				<div style={{ marginBottom: '20px' }}>
					<Text>Заключение:</Text>
					<Text style={styles.text}>{props.conclusion}</Text>
				</div>
			</Page>
		</Document>
	)
}

export default PDFfile