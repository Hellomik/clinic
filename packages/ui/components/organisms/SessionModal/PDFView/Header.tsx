import React, { useContext } from "react";

import { Link, Text, View, StyleSheet } from "@react-pdf/renderer";
import { SessionMetadataContext } from "..";
import { User } from "@lucem/shared-gql";
import { format } from "date-fns";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    borderBottomWidth: 2,
    borderBottomColor: "#112131",
    borderBottomStyle: "solid",
    alignItems: "stretch",
  },
  detailColumn: {
    flexDirection: "column",
    flexGrow: 9,
    textTransform: "uppercase",
  },
  linkColumn: {
    flexDirection: "column",
    flexGrow: 2,
    alignSelf: "flex-end",
    justifySelf: "flex-end",
  },
  name: {
    fontSize: 24,
    fontFamily: "Lato Bold",
  },
  subtitle: {
    fontSize: 10,
    justifySelf: "flex-end",
    fontFamily: "Lato",
  },
  link: {
    fontFamily: "Lato",
    fontSize: 10,
    color: "black",
    textDecoration: "none",
    alignSelf: "flex-end",
    justifySelf: "flex-end",
  },
});
const Header = ({ patient }: { patient: User }) => {
  return (
    <View style={styles.container}>
      <View style={styles.detailColumn}>
        <Text style={styles.name}>{patient?.fullName}</Text>
        <Text style={styles.subtitle}>
          Дата рождения:{" "}
          {patient?.dateOfBirth &&
            format(new Date(patient?.dateOfBirth), "dd.MM.yyyy")}
        </Text>
      </View>
      <View style={styles.linkColumn}>
        <Link href="mailto:luke@theforce.com" style={styles.link}>
          {patient?.email}
        </Link>
      </View>
    </View>
  );
};

export default Header;
