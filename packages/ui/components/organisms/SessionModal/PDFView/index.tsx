import React, { useContext } from "react";
import {
  Text,
  Font,
  Page,
  View,
  Image,
  Document,
  StyleSheet,
} from "@react-pdf/renderer";
import { PDFViewer } from "@react-pdf/renderer";
import Header from "./Header";
import Skills from "./Skills";
import Education from "./Education";
import Experience from "./Experience";
import { useFormikContext } from "formik";
import { SessionMetadataContext } from "..";
import { User } from "@lucem/shared-gql";
import { CreateAppointmentBlankFormSchema } from "../shared/initialValues";
import { others } from "@chakra-ui/react";

const styles = StyleSheet.create({
  page: {
    padding: 30,
  },
  container: {
    flex: 1,
    flexDirection: "column",
    "@media max-width: 400": {
      flexDirection: "column",
    },
  },
  image: {
    marginBottom: 10,
  },
  leftColumn: {
    flexDirection: "column",
    width: 170,
    paddingTop: 30,
    paddingRight: 15,
    "@media max-width: 400": {
      width: "100%",
      paddingRight: 0,
    },
    "@media orientation: landscape": {
      width: 200,
    },
  },
  footer: {
    fontSize: 12,
    fontFamily: "Open Sans",
    textAlign: "center",
    marginTop: 15,
    paddingTop: 5,
    borderWidth: 3,
    borderColor: "gray",
    borderStyle: "dashed",
    "@media orientation: landscape": {
      marginTop: 10,
    },
  },
});

Font.register({
  family: "Open Sans",
  src: `https://fonts.gstatic.com/s/opensans/v34/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0C4nY1M2xLER.ttf`,
});

Font.register({
  family: "Lato",
  src: `https://fonts.gstatic.com/s/opensans/v34/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0C4nY1M2xLER.ttf`,
});

Font.register({
  family: "Lato Italic",
  src: `https://fonts.gstatic.com/s/opensans/v34/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkaVcUwaERZjA.ttf`,
});

Font.register({
  family: "Lato Bold",
  src: `https://fonts.gstatic.com/s/opensans/v34/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1y4nY1M2xLER.ttf`,
});

export interface PDFData {
  data: { patient: User; values: CreateAppointmentBlankFormSchema };
}

const Resume = (props: PDFData) => {
  const { data, ...others } = props;
  return (
    <Page {...others} style={styles.page}>
      <Header patient={data.patient} />
      <View style={styles.container}>
        <Experience values={data.values} />
      </View>
      <Text style={styles.footer}>
        This IS the candidate you are looking for
      </Text>
    </Page>
  );
};

const PDFView = () => {
  const { values } = useFormikContext();
  const { patient } = useContext(SessionMetadataContext);

  return (
    <div>
      <PDFViewer width="100%" height={1000}>
        <Document
          author="Luke Skywalker"
          keywords="awesome, resume, start wars"
          subject="The resume of Luke Skywalker"
          title={"Прием " + patient?.fullName}
        >
          <Resume data={{ patient, values }} size="A4" />
        </Document>
      </PDFViewer>
    </div>
  );
};

export default PDFView;
